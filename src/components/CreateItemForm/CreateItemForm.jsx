import { useState } from 'react';

const CreateItemForm = ({updateList, list}) => {
    const [newItem, setNewItem] = useState('');

    const handleChange = (event) => {
        // 👇 Get input value from "event"
        setNewItem(event.target.value);
      };

    const insertNewItem = () => {
        list.push({key : newItem, value : true})
        updateList(list)
    };

    return (
    <div>
        <input onChange={handleChange} />
        <button onClick={insertNewItem}>Créer</button>
        </div>
    )
  }
  
  export default CreateItemForm