import { useState } from 'react';

const ToDoList = ({elements}) => {

  return (
    <div>
        {elements.map((el) => {     
            return (<div><p>{el.key}</p><input checked={el.value} type="checkbox" /></div>) 
        })}
     </div>
  )
}

export default ToDoList