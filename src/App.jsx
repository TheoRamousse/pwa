import { useState, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import CreateItemForm from './components/CreateItemForm/CreateItemForm'
import ToDoList from './components/ToDoList/ToDoList'
import React from 'react';
import { pwa } from "pwafire";

function App() {
  const [list, setList] = useState([])
  const [listContacts, setListContacts] = useState([])

  const asyncFunction = async () => {
    const props = ["name"];
    const options = { multiple: true };

    try{
      const res = await pwa.Contacts(props, options);
      alert(res.contacts)
      setListContacts(res.contacts)
    }catch(e){
      alert(e)
    }
  }


  const updateList = (el) => {
    var newList = [...el]
    setList(newList)
  }

  return (
    <div className="App">
      <h2>Choses to do :</h2>
      <ToDoList elements={list} />
      <CreateItemForm list={list} updateList={updateList} />


      <button onClick={asyncFunction}>Coca Cola</button>
      <div>
        {listContacts.map((el) => {     
            return (<div><p>{el.name}</p></div>) 
        })}
     </div>

    </div>
  );
}

export default App;