import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
        react(),
        VitePWA({
                manifest: {
                  name: 'vite-react-js-100',
                  short_name: 'vite-react-js-100',
                  start_url: '/',
                  display: 'standalone',
                  background_color: '#ffffff',
                  lang: 'en',
                  scope: '/',
                  icons: [
                    {
                      src: '/icon-192x192.png',
                      sizes: '192x192',
                      type: 'image/png',
                      purpose: 'any maskable',
                    },
                    {
                      src: '/icon-512x512.png',
                      sizes: '512x512',
                      type: 'image/png',
                      purpose: 'any maskable',
                    },
                  ],
                  theme_color: '#ffffff',
                },
              }),
      ],
})

